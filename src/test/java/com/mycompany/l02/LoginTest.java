package com.mycompany.l02;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class LoginTest extends BaseTest {

    @Test(description = "Login Page - valid credentials")
    public void login() {
        //login field
        WebElement userNameInput = driver.findElement(By.id("username"));
        userNameInput.sendKeys("Test1");

        //password field
        WebElement passwordInput = driver.findElement(By.id("password"));
        passwordInput.sendKeys("Studen123456!");

        //click login button
        WebElement loginButton = driver.findElement(By.xpath("//button[@name='login']"));
        loginButton.click();

        assertEquals(driver.findElements(By.cssSelector("ul.woocommerce-error > li")).size(), 0);
    }

    @Test(description = "Login Page - invalid credentials")
    public void loginNegative() {
        //login field
        WebElement userNameInput = driver.findElement(By.id("username"));
        userNameInput.sendKeys("GutenMorgen@testmail.com");

        //password field
        WebElement passwordInput = driver.findElement(By.id("password"));
        passwordInput.sendKeys("bla-bla-bla");

        //click login button
        WebElement loginButton = driver.findElement(By.xpath("//button[@name='login']"));
        loginButton.click();

        assertTrue(driver.findElement(By.cssSelector("ul.woocommerce-error > li")) != null);
    }
}
