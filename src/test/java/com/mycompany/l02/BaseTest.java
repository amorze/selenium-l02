package com.mycompany.l02;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest {
    WebDriver driver;

    @BeforeMethod
    public void initData() {
        driver = new ChromeDriver();
        driver.get("http://cw07529-wordpress.tw1.ru/my-account/");
        driver.manage().window().maximize();
    }

    @AfterMethod
    public void stop() {
        driver.quit();
    }

}
